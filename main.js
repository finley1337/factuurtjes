const {app, BrowserWindow} = require('electron');

let win;

CreateWindow = () => {

    win = new BrowserWindow({
        width: 800,
        height: 600
    });

    win.loadURL(`file://${__dirname}/dist/index.html`);

    win.webContents.openDevTools();

    win.on('closed', () => {
        win = null;
    });
};

app.on('ready', CreateWindow);

app.on('window-all-closed', () => {

    if(process.platform !== 'darwin') {
        app.quit();
    }

});

app.on('active', () => {

    if(win === null) {
        CreateWindow();
    }

});
