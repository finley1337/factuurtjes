import {Component, OnInit} from '@angular/core';
import * as jsPDF from 'jspdf';


const settings = {
    'personal': {
        'name': 'WSB',
        'address': 'Bleekstraat 12',
        'postal': '7416 AB',
        'city': 'Deventer',
        'cellphone': '0653731744'
    },
    'business': {
        'invoice': '2485',
        'deb': '0084',
        'btw': 'NL853803328B01',
        'tax': '21',
        'iban': 'NL88INGB.065.42.35.112'
    }

    // 'personal': {
    //     'name': 'FHD',
    //     'address': 'Aan Hiemstenrade 13',
    //     'postal': '6134 RB',
    //     'city': 'Sittard',
    //     'cellphone': '065746574634'
    // },
    // 'business': {
    //     'invoice': '1234',
    //     'deb': '4321',
    //     'btw': 'NL123437846B34',
    //     'tax': '21',
    //     'iban': 'NL33INGB0002434232'
    // }
};

@Component({
    selector: 'app-new-invoice',
    templateUrl: './new-invoice.component.html',
    styleUrls: ['./new-invoice.component.scss']
})
export class NewInvoiceComponent implements OnInit {

    configuration = settings;
    customers = [
        {
            name: 'Amcor Flexibles Zutphen',
            address: 'Finsestraat 1',
            zip: '7202 CE',
            city: 'Zutphen',
            btw: 'NL001632735B01'
        },
        {
            name: 'Finley siebert BV',
            address: 'Mgr. Canoystraat 40',
            zip: '6134 RB',
            city: 'Sittard',
            btw: 'NL0014345354D02'
        }
    ];

    productamount = '1';
    productinfo: string;
    productprice = '0,01';

    personalname = settings.personal.name;
    personaladdress = settings.personal.address;
    personalpostal = settings.personal.postal;
    personalcity = settings.personal.city;
    personalcellphone = settings.personal.cellphone;

    personalinvoice = settings.business.invoice;
    personaldeb = settings.business.deb;
    personalbtw = settings.business.btw;
    personaliban = settings.business.iban;
    date: any = new Date().toLocaleDateString();

    customername = '';
    customeraddress = '';
    customerpostal = '';
    customercity = '';
    customerbtw = '';

    customerdata = 0;

    customerExistsError = false;
    customerError;

    error = false;

    products = [];

    constructor() {
    }

    ngOnInit() {

        this.customers.forEach(x => {
            console.log(x);
        });
    }

    generatePdf() {
        const pdf = new jsPDF();

        pdf.setFontSize(12);

        pdf.text(20, 50, this.personalname);
        pdf.text(20, 57, this.personaladdress);
        pdf.text(20, 64, this.personalpostal + ' ' + this.personalcity);
        pdf.text(20, 71, 'TELEFOON' + ' ' + this.personalcellphone);

        pdf.text(120, 81, this.customername);
        pdf.text(120, 88, this.customeraddress);
        pdf.text(120, 95, this.customerpostal + ' ' + this.customercity);

        pdf.setFontType('bold');
        pdf.text(20, 115, 'Gelieve bij betaling te vermelden');

        pdf.setFontType('normal');

        pdf.text(20, 125, 'Rekening nr. ' + this.personalinvoice);
        pdf.text(20, 135, 'Deb nr. ' + this.personaldeb);
        pdf.text(20, 142, 'Datum: ' + this.date);
        pdf.text(20, 149, 'BTW nr. ' + this.personalbtw);

        pdf.text(20, 156, '________________________________________________________________________');

        pdf.text(20, 165, 'BTW nr. ' + this.customerbtw);

        let height = 179;

        this.products.forEach(product => {

            pdf.text(20, height, product.amount + ' x ' + product.info);
            pdf.text(160, height, '€ ' + parseInt(product.price).toLocaleString());

            height = height + 7;
        });

        pdf.text(160, height + 14, '€ ' + this.calculateFull().toLocaleString());
        pdf.text(20, height + 14, 'Totaal: ');

        pdf.text(160, height + 21, '€ ' + this.calculateTax().toLocaleString());
        pdf.text(20, height + 21, 'BTW (' + settings.business.tax + '%):');

        pdf.text(20, height + 25, '________________________________________________________________________');

        pdf.text(160, height + 38, '€ ' + this.calculateTotal().toLocaleString());
        pdf.text(20, height + 38, 'Te betalen: ');


        pdf.save(this.customername + '_Factuur_' + this.date + '.pdf');
    }

    calculateTax() {

        let fullprice = 0;
        this.products.forEach(x => {
            fullprice += x.price;
        });

        return Math.round((fullprice * (parseInt(settings.business.tax) / 100)) * 100) / 100;

    }

    calculateFull() {
        let fullprice = 0;
        this.products.forEach(x => {
            fullprice += x.price;
        });

        return Math.round(fullprice * 100) / 100;
    }

    addProduct() {


        if (this.productinfo.length < 1 || parseInt(this.productamount) < 1 || parseFloat(this.productprice) < 0.01) {
            this.error = true;
        }
        else {
            this.error = false;

            if(this.productinfo.length >= 60) {
                this.productinfo[60] += '\r'; 
            }

            const p = {
                'info': this.productinfo,
                'amount': this.productamount,
                'price': Math.round((parseFloat(this.productprice) * parseInt(this.productamount)) * 100) / 100,
                'price_pp': Math.round((parseFloat(this.productprice) * 100)) / 100
            };

            this.products.push(p);

            this.productinfo = '';
            this.productamount = '1';
            this.productprice = '0,01';


        }
    }

    deleteProduct(name, price) {

        for (let i = 0; i < this.products.length; i++) {

            if (this.products[i].info === name && this.products[i].price === price) {

                console.log(parseInt(this.products[i].amount));

                if (parseInt(this.products[i].amount) > 1) {
                    this.products[i].price = Math.round((parseFloat(this.products[i].price) - parseFloat(this.products[i].price_pp)) * 100) / 100;
                    this.products[i].amount--;
                }
                else {
                    this.products.splice(i, 1);
                }
            }

        }

    }

    calculateTotal() {

        return Math.round((this.calculateFull() + this.calculateTax()) * 100) / 100;

    }

    deleteFull(name, price) {
        for (let i = 0; i < this.products.length; i++) {
            if (this.products[i].info === name && this.products[i].price === price) {
                this.products.splice(i, 1);
            }
        }
    }

    deleteCart() {

        const c = confirm('Weet je zeker dat je de hele productenlijst wilt legen?');

        if (c) {
            this.products = [];
        }

    }

    autoFillCompany() {

        this.customers.forEach(customer => {

            if (customer.btw == this.customerdata) {

                this.customername = customer.name;
                this.customeraddress = customer.address;
                this.customercity = customer.city;
                this.customerpostal = customer.zip;
                this.customerbtw = customer.btw;

            }

        });

    }

    clearCustomer() {

        this.customername = '';
        this.customeraddress = '';
        this.customerpostal = '';
        this.customercity = '';
        this.customerbtw = '';

        this.customerdata = 0;

    }

    newCustomer() {


        if (this.customername === '' || this.customeraddress === '' || this.customerpostal === '' || this.customerbtw === '' || this.customercity === '') {
            this.customerError = true;
        }
        else
        {
            this.customers.forEach(customer => {
                if(this.customerbtw == customer.btw) {
                    this.customerError = false;
                    this.customerExistsError = true;
                }
            });

            if(!this.customerExistsError) {

                this.customerError = false;

                const c = {
                    name: this.customername,
                    address: this.customeraddress,
                    zip: this.customerpostal,
                    city: this.customercity,
                    btw: this.customerbtw,
                };

                this.customers.push(c);
            }
        }

    }

}
