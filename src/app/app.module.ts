import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';


import { AppComponent } from './app.component';
import {Route, RouterModule} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NewInvoiceComponent } from './new-invoice/new-invoice.component';
import {FormsModule} from '@angular/forms';
import { PdfViewComponent } from './pdf-view/pdf-view.component';

const routes: Route[] = [
    { path: '', component: HomeComponent },
    { path: 'nieuw-factuur', component: NewInvoiceComponent },
    { path: 'pdf-export', component: PdfViewComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NewInvoiceComponent,
    PdfViewComponent
  ],
  imports: [
      BrowserModule,
      RouterModule.forRoot(routes),
      FormsModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
